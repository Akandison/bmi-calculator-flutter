import 'package:flutter/material.dart';
import 'package:bmi/Constants.dart';

class MyCard extends StatelessWidget {
  final String genderText;
  final IconData genderIcon;
  MyCard({this.genderText, this.genderIcon});
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(genderIcon, size: 50),
        SizedBox(height: 15),
        Text(
          genderText,
          style: kLabelTextStyle,
        ),
      ],
    );
  }
}
