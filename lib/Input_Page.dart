import 'package:bmi/Results.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'MyContainer.dart';
import 'MyCard.dart';
import 'package:bmi/Constants.dart';
import 'ButtomDesign.dart';
import 'bmiCalc.dart';

enum GenderListing { Female, Male }

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  Color maleCardColor = kContainerInactiveColor;
  Color femaleCardColor = kContainerInactiveColor;
  GenderListing selectedGender;
  int _height = 140;
  double maxVal;
  int weight = 60;
  int age = 0;

  /*void updateColor(GenderListing selectedGender) {
    selectedGender == GenderListing.Male &&
            maleCardColor == containerActiveColor
        ? maleCardColor = containerInactiveColor
        : maleCardColor = containerActiveColor;

    // 1=male card pressed
    /*  if (selectedGender == GenderListing.Male) {
      if (maleCardColor == containerActiveColor) {
        maleCardColor = containerInactiveColor;
      } else {
        maleCardColor = containerActiveColor;
        femaleCardColor = containerInactiveColor;
      }
    }

   */
    selectedGender == GenderListing.Female &&
            femaleCardColor == containerActiveColor
        ? femaleCardColor = containerInactiveColor
        : femaleCardColor = containerActiveColor;
    //female card pressed
    if (selectedGender == GenderListing.Female) {
      if (femaleCardColor == containerActiveColor) {
        femaleCardColor = containerInactiveColor;
      } else {
        femaleCardColor = containerActiveColor;
        maleCardColor = containerInactiveColor;
      }
    }
  }*/

  @override
  Widget build(BuildContext context) {
    BmiCal bmiCalcObj = BmiCal(weight: weight, height: _height);
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('BMI CALCULATOR')),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: MyContainer(
                    onPress: () {
                      setState(() {
                        selectedGender = GenderListing.Male;
                      });
                    },
                    myChild: MyCard(
                      genderText: "MALE",
                      genderIcon: FontAwesomeIcons.mars,
                    ),
                    myColor: selectedGender == GenderListing.Male
                        ? kContainerActiveColor
                        : kContainerInactiveColor,
                  ),
                ),
                Expanded(
                  child: MyContainer(
                    onPress: () {
                      setState(() {
                        selectedGender = GenderListing.Female;
                      });
                    },
                    myChild: MyCard(
                        genderText: "FEMALE",
                        genderIcon: FontAwesomeIcons.venus),
                    myColor: selectedGender == GenderListing.Female
                        ? kContainerActiveColor
                        : kContainerInactiveColor,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: MyContainer(
              myColor: kContainerActiveColor,
              myChild: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("HEIGHT", style: kLabelTextStyle),
                  Row(
                    textBaseline: TextBaseline.alphabetic,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    children: <Widget>[
                      Text(
                        _height.toString(),
                        style: kValueTextStyle,
                      ),
                      Text("CM", style: kLabelTextStyle),
                    ],
                  ),
                  SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                        thumbShape:
                            RoundSliderThumbShape(enabledThumbRadius: 15),
                        overlayShape:
                            RoundSliderOverlayShape(overlayRadius: 28),
                        thumbColor: Color(0xFFEB1555),
                        activeTrackColor: Colors.white,
                        inactiveTrackColor: Color(0xFF8D8E98),
                        overlayColor: Color(0x29EB1555)),
                    child: Slider(
                      value: _height.toDouble(),
                      onChanged: (double newVal) {
                        print(newVal);
                        setState(() {
                          _height = newVal.round();
                        });
                      },
                      min: 120.0,
                      max: 250.0,

                      //   max: 240
                      //
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: MyContainer(
                    myColor: kContainerActiveColor,
                    myChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("WEIGHT", style: kLabelTextStyle),
                        Text(
                          weight.toString(),
                          style: kValueTextStyle,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            FloatingActionButton(
                              heroTag: "btn1",
                              backgroundColor: Color(0xFF4C4F5E),
                              child: Icon(
                                FontAwesomeIcons.plus,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  weight++;
                                });
                              },
                            ),
                            SizedBox(width: 10),
                            FloatingActionButton(
                              backgroundColor: Color(0xFF4C4F5E),
                              child: Icon(
                                FontAwesomeIcons.minus,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  weight--;
                                });
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: MyContainer(
                    myColor: kContainerActiveColor,
                    myChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Age", style: kLabelTextStyle),
                        Text(
                          age.toString(),
                          style: kValueTextStyle,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            FloatingActionButton(
                              heroTag: "btn2",
                              backgroundColor: Color(0xFF4C4F5E),
                              child: Icon(
                                FontAwesomeIcons.plus,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  age++;
                                });
                              },
                            ),
                            SizedBox(width: 10),
                            FloatingActionButton(
                              heroTag: "btn3",
                              backgroundColor: Color(0xFF4C4F5E),
                              child: Icon(
                                FontAwesomeIcons.minus,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  if (age > 0) age--;
                                });
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          ButtomButton(
            title: "CALCULATE",
            unPresso: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Results(
                            bmiVal: bmiCalcObj.calc(),
                            bmiResults: bmiCalcObj.bmiResults(),
                            interpretation: bmiCalcObj.bmiExplainer(),
                          )));
            },
          ),
        ],
      ),
    );
  }
}

class AgeWeight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
