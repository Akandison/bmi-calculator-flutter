import 'package:flutter/material.dart';
import 'package:bmi/Constants.dart';

class ButtomButton extends StatelessWidget {
  final Function unPresso;
  final String title;
  ButtomButton({@required this.unPresso, @required this.title});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: unPresso,
      child: Container(
        margin: EdgeInsets.only(top: 10),
        padding: EdgeInsets.only(bottom: 20),
        color: kPinkColorBottom,
        width: double.infinity,
        height: kBottomContainerHeight,
        child: Center(
          child: Text(
            title,
            style: kLargeButtonStyle,
          ),
        ),
      ),
    );
  }
}
