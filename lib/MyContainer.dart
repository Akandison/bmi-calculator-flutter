import 'package:flutter/material.dart';

class MyContainer extends StatelessWidget {
  final Color myColor;
  final Widget myChild;
  final Function onPress;
  MyContainer({@required this.myColor, this.myChild, this.onPress});
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        child: myChild,
        margin: EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: myColor,
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
