import 'package:bmi/Constants.dart';
import 'package:flutter/material.dart';
import 'MyContainer.dart';
import 'ButtomDesign.dart';

class Results extends StatelessWidget {
  final String bmiVal;
  final String bmiResults;
  final String interpretation;
  Results(
      {@required this.bmiVal,
      @required this.bmiResults,
      @required this.interpretation});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text("BMI CALCULATOR"),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(15),
              alignment: Alignment.bottomLeft,
              child: Text(
                "YOUR RESULT",
                style: kTitleTextStyle,
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: MyContainer(
              myColor: kContainerActiveColor,
              myChild: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(bmiResults.toUpperCase(), style: kResultsStyle),
                  Text(
                    bmiVal,
                    style: kBMIresults,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    interpretation,
                    style: kInterpretate,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: ButtomButton(
              title: "RECALCULATE",
              unPresso: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}
