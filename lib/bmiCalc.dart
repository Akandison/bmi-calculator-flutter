import 'dart:math';

import 'package:flutter/cupertino.dart';

class BmiCal {
  BmiCal({@required this.weight, @required this.height});
  final int weight;
  final int height;
  double _bmi;

  calc() {
    _bmi = weight / pow(height / 100, 2);
    return _bmi.toStringAsFixed(1);
  }

  bmiResults() {
    if (_bmi >= 25) {
      return "Overweight";
    } else if (_bmi >= 18.5) {
      return "Normal";
    } else {
      print(_bmi);
      return "Underweight";
    }
  }

  bmiExplainer() {
    if (_bmi >= 25) {
      return "Exercise more";
    } else if (_bmi >= 18.5) {
      return "BMI normal,well done";
    } else {
      return "Eat a bit more";
    }
  }
}
