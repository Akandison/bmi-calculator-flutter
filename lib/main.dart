import 'package:flutter/material.dart';
import 'Input_Page.dart';

void main() => runApp(BMICalculator());

class BMICalculator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
        textTheme: TextTheme(
          body1: TextStyle(color: Colors.white),
        ),
        primaryColor: Color(0xFF111639),
        scaffoldBackgroundColor: Color(0xFF0E1337),
      ),
      home: InputPage(),
    );
  }
}
