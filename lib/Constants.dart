import 'package:flutter/material.dart';

const kBottomContainerHeight = 80.0;
const Color kContainerActiveColor = Color(0xFF2A2A2C);
const kPinkColorBottom = Color(0xFFFF0066);
const Color kContainerInactiveColor = Color(0xFF111328);

double minValSlider = 120;
double maxValSlider = 220;

const kValueTextStyle = TextStyle(
  fontWeight: FontWeight.w900,
  fontSize: 50,
);

const kLabelTextStyle = TextStyle(
  color: Color(0xFF808E98),
  fontSize: 18,
);
const kLargeButtonStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 25,
);
const kTitleTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 50.0,
);
const kResultsStyle = TextStyle(
  color: Color(0xFF24D876),
  fontSize: 22,
  fontWeight: FontWeight.bold,
);

const kBMIresults = TextStyle(
  fontSize: 100,
  fontWeight: FontWeight.bold,
);
const kInterpretate = TextStyle(
  fontSize: 22,
);
